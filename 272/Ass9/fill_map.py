import sys
import xml.etree.ElementTree as ET
import matplotlib.cm as mcm
import matplotlib.colors as mclrs

csv = sys.argv[1]
svgmap = sys.argv[2]

def main(info, maximum):
	tree = ET.parse(svgmap)
	root = tree.getroot()
	gselect = r'{http://www.w3.org/2000/svg}g'
	
	paths = []
	for p in root.iter(gselect):
			paths.append(p)
	for path in paths:
		try:
			scale = (info[path.attrib['id']]/maximum)
			path.attrib['style'] = 'fill:' + mclrs.rgb2hex(mcm.OrRd(scale))
		except KeyError:
			continue
	
	tree.write('out1.svg')

def color_data():
	data = open(csv, 'r')
	data.readline()
	maxi = 0
	info = {}
	l = []
	for line in data:
		l = line.strip('\n').split(',')
		info[l[1]] = int(l[6])
		if maxi < int(l[6]):
			maxi = int(l[6])
	return info, maxi

if __name__ == '__main__':
	A = color_data()
	main(A[0], A[1])
