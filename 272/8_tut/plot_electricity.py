import numpy as np
import matplotlib.pyplot as plt

def plot():
	data = [('BFN', 61, 91), ('CPT', 87, 94), ('DBN', 74, 90), ('ELN', 47, 81),
	('GER', 75, 82), ('JHB', 85, 91), ('PEZ', 71, 90), ('PTA', 77, 89)]
	width = 1.0
	x = np.arange(0, len(data))
	y1 = [num for (s, num, z) in data]
	y2 = [z for (s, num, z) in data]
	labels = [s for (s, x, z) in data]

	plt.bar(x+0.35, y1, 0.35, color='r')
	plt.bar(x, y2, 0.35, color='g')
	plt.ylabel("Eskom")
	plt.xticks(x + 0.175, labels)
	plt.show()

if __name__ == '__main__':
	plot()
