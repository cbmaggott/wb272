import sys
import csv

in_file = sys.argv[1]
out_file = sys.argv[2]

def process_file():
	with open(in_file) as data_file:
		data_reader = csv.reader(data_file, delimiter=',')
		line_count = 0
		total, marks = [], []
		for line in data_reader:
			if (line_count == 1):
				total = line
				line_count += 1
			elif (line_count > 1):
				marks.append(line)
				marks[line_count - 2].append(fmark(total, line[1:9], line[9:]))
				line_count += 1
			else:
				line_count += 1
	

def fmark(total, assgn, tests):
	assignments, t_frac, t_marks = 0, 0, [0, 0, 0, 0]
	
	for t, s, in zip(total[1:9], assgn):
		if (s == ''):
			s = 0
		assignments += int(s) / int(t)

	for t, i in zip(total[9:], range(len(tests))):
		if (tests[i] == ''):
			tests[i] = 0
		t_marks[i] = int(tests[i]) / int(t)

	assignments = (assignments / 8) * 64
	t_marks.sort()
	t_marks.reverse()
	t_frac = (sum(t_marks[:3]) / 3) * 36
	f_mark = assignments + t_frac
	
	if (35 <= f_mark <= 50):
		f_mark = round5(f_mark)
	else:
		f_mark = round(f_mark)
	
	return f_mark

def round5(x):
	y = x % 10
	if (y < 2.5 or 5 <= y < 7.5):
		x -= x % 5
	elif (2.5 <= y < 5):
		x += 5 - (x % 5)
	elif (7.5 <= y < 10):
		x += 10 - (x % 10)
	return int(x)

if __name__ == '__main__':
	process_file()
