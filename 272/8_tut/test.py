def round_to_5(x):
	y = x % 10
	if (y < 2.5 or 5 <= y < 7.5):
		x = x - (x % 5)
	elif (2.5 <= y < 5):
		x = x + (5 - x % 5)
	elif (7.5 <= y < 10):
		x = x + (10 - x % 10)
	return x
