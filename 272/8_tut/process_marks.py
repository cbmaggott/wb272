import sys
import numpy as np
import matplotlib.pyplot as plt

input_f = sys.argv[1]
output_f = sys.argv[2]
N = 20
hist = [0] * N

def process():
	data, total, final_mark= "", [], 0
	f = open(input_f, 'r')
	out_file = open(output_f, 'w+')
	header = f.readline() + ",Final Mark\n"
	out_file.write(header)
	total = f.readline().replace("\n", "").split(",")
	
	for item in total:
		out_file.write(item)
		out_file.write(",")

	out_file.write("100\n")

	for line in f:
		data = line.replace("\n", "")
		final_mark = fmark(data, total)
		if 35 <= final_mark <= 50:
			data += ","	+ str(round5(final_mark))
			hist[int(round5(final_mark) // (100//N))] += 1
		else:
			data += "," + str(round(final_mark))
			hist[int(round(final_mark) // (100//N))] += 1
		out_file.write(data + '\n')

	out_file.close()
	f.close()

def plot():
	width = 1.0
	plt.bar(np.arange(0, N), hist, width/2, color="b")
	plt.xlabel("Bins")
	plt.ylabel("Percentage")
	plt.xticks(np.arange(0, N))
	plt.show()

def fmark(data, total):
	d_array = data.split(",")
	marks, T_mark = 0, 0
	for i in range(1, 9):
		if d_array[i] == '':
			d_array[i] = 0
		marks += int(d_array[i]) / int(total[i])
	
	T_mark += (marks / 8) * 64

	marks = 0

	d2_array = []

	for i in range(9, 13):
		if d_array[i] == '':
			d_array[i] = 0
		d2_array.append(int(d_array[i]) / int(total[i]))

	d2_array.sort()
	d2_array.reverse()
	d2_array.pop()
	T_mark += (sum(d2_array) / 3) * 36
	
	return T_mark

def round5(x):
	y = x % 10
	if (y < 2.5 or 5 <= y < 7.5):
		x -= x % 5
	elif (2.5 <= y < 5):
		x += 5 - (x % 5)
	elif (7.5 <= y < 10):
		x += 10 - (x % 10)
	return int(x)



if __name__ == '__main__':
	process()
	plot()
