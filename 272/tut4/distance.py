def closest(coords):
	dist = []
	for i in coords:
		dist.append(pow(pow(i[0], 2)+pow(i[1], 2),0.5))
	min_dist = min(dist)
	index = dist.index(min_dist)
	return coords[index], index, min_dist
