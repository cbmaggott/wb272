import sys
def is_valid_usnum(num):
	length = len(num) + 1
	checksum = 0
	for i in range(1, length):
		checksum += (i*int(num[-i]))
	return print((checksum % 11) == 0)

if __name__ == '__main__':
	is_valid_usnum(sys.argv[1])
