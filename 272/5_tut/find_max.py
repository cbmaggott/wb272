def fmax(L, i):
	'''Finding the max of a list'''
	max_ele = L[i]
	max_index = i
	for c in range(i, len(L)):
		if L[c] > max_ele:
			max_ele = L[c]
			max_index = c
	return max_index-i
