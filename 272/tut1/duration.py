def elapsed_minutes(start, end):
	start_min = int(start[:2]) + int(start[2:])/60
	end_min = int(end[:2]) + int(end[2:])/60
	return int((end_min - start_min) * 60)
