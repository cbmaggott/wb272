import nose 
from days import day_of_the_week

def test_sunday():
	"""Failed for Sunday"""
	assert day_of_the_week(2019, 05, 26) == 0

def test_monday():
	"""Failed for monday"""
	assert day_of_the_week(2019, 05, 27) == 1

def test_wednesday():
	"""Failed for wednesday"""	
	assert day_of_the_week(2019, 05, 29) == 3

def test_thursday():
	"""Failed for thursday"""
	assert day_of_the_week(2019, 05, 30) == 4

def test_friday():
	"""Failed for friday"""
	assert day_of_the_week(2019, 05, 31) == 5


if __name__ == '__main__':
	nose.runmodule()
