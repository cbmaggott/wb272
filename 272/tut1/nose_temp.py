import nose
from temperature import to_celsius
from temperature import celsius_to_fahrenheit

def test_to_celsius():
	"""Testing celsius conversion"""
	assert to_celsius(32) == 0

def test_to_fahrenheit():
	"""Testing fahreheit conversion and just looking different"""
	assert celsius_to_fahrenheit(0) == 32

if __name__ == '__main__':
	nose.runmodule()
