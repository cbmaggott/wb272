def print_as_vector(L):
	print("<{0}".format(L[0]), end="")
	
	for i in range(1, len(L)):
		print(", {0}".format(L[i]), end="")
	
	print(">")
