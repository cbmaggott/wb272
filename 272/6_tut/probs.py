def factorial(n):
	if n == 1:
		return n
	else:
		return n * factorial(n - 1)

def permutation(n, k):
	return factorial(n) / factorial(n - k)

def stdev(L):
	N = len(L)
	x, u = 0, 0
	for i in range(N):
		x += pow(L[i], 2)
		u += L[i]
	return pow((x/N - pow(u/N, 2)), 0.5)
