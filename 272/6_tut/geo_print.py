def parallelogram(L):
	L = L.upper()
	s = 2 * -len(L) + 1
	span = len(L)
	k = " "
	for i in range(1, 2 * len(L)):
		print("{0:>{a}}".format(L[s:i], a=span))
		L += k

def triangle(n):
	dilly(n)
	x = 'X'
	y = ""
	for i in range(n):
		y += x
		print(y)

def triangle2(n):
	dilly(n)
	c = 1
	x = ""
	for i in range(n):
		for j in range(c):
			x += "X"
		print(x)
