import math
def mercator(lam0, lam, phi):
	x = lam - lam0
	y = 0.5 * math.log((1+math.sin(math.radians(phi)))/(1-math.sin(math.radians(phi))))
	return x,y
