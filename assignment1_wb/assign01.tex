% Text wrap manually set to 80
\documentclass[10pt, a4paper]{article}
\usepackage{amsmath}

\title{\LaTeX{} Assignment 1: Basic Text and Mathematics}
\author{C.B. Maggott, 20941390}
\date{10 March 2020}

\begin{document}
\maketitle

\section{Instructions}
Type \emph{exactly} what you see here.\footnote{Remember that we specify logical
markup in \LaTeX{}. So, we do not use the \texttt{textit} command to emphasise
text.} Use the \texttt{article} class with the \texttt{10pt} and \texttt{a4paper}
options; \emph{do not use any other package.} There are, however, two required
changes: Substitute your initials, surname, and student number for mine in the
title; and set the date to the due date---be careful not to use the ``current''
system date.

Most of the examples are a bit forced. We'll see things like \underline{underline
text}, which we use rarely outside math mode. However, we'll worry about
\textbf{substance} -- and \textbf{style!} -- later.\footnote{Pay attention to
unwanted end-of-sentence spacing in this sentence. Actually, check
end-of-sentence punctuation through out the document.}

\paragraph{Submission} The results for these assignments constitute your mark for
the \LaTeX{} part of the module.\footnote{The ``heading'' for this paragraph was
made with the \texttt{paragraph} command.} Refer to the course website for
details. \textsc{In particular, note}, that you have to submit both your
\texttt{.tex} and \texttt{.pdf} files as an archive on the module website, and
that you have to
\emph{print out, complete, sign, and submit the plagiarism form to the lecturer.}
if you do not follow all the submission instructions, your submission will not be
marked. If your submitted \texttt{.tex} file does not compile successfully on
Ubuntu in \textsc{Narga}, you get zero.

\section{Mathematics}
One of the many string suits of \TeX{} and \LaTeX{} is how well they handle the
typesetting of mathematics. Of course, to start, many of the equations in
\S\ref{sec:symbols}, \S\ref{sec:sas}, \S\ref{sec:operations},
\S\ref{sec:fractions}, and \S\ref{sec:mms} are complete nonsence.\footnote{The
numeric referenes in this sentence were made automatically with the
\texttt{label} and \texttt{ref} commands. It is a good idea to preface the
different classes of labels with class-identifying strings, each followed by a
colon. For example, I have used ``\texttt{sec:}'' for section labels, so that the
full label for \S2.4 is `\texttt{sec:fractions}'.}

\subsection{Symbols}
\label{sec:symbols}
To many people, mathematics is Greek. Up to a point, they are correct: We do use a lot of
Greek symbols, for example, $\alpha$, $\beta$, $\Gamma$, $\Delta$, $\chi$, and
$\Omega$. Note that the spaces before and after the commas in the previous list
imply that \emph{we do not use only one math environment.}

\indent Let's do a bit of Physics. Everybody should know about the time-dependent
Schr\"{o}dinger equation:
$$i\hbar \frac{\partial}{\partial t}\Psi = \widehat{H} \Psi.$$

\noindent A famous example is the non-relativistic Schr\"{o}dinger equation for a
single particle moving in an electric field:

$$ i\hbar \frac{\partial}{\partial t}\Psi(\mathbf{r},t) =
\left[\frac{-\hbar^2}{2\mu}\nabla^2 + V(\mathbf{r},t)\right] \Psi(\mathbf{r},
t).$$

\indent For diplayed mathematics, we keep the punctuation inside the math
environment, but for mathematics in running text, we put the punctuation outside
the math environment. Keep and eye out for paragraph breaks. It should be as easy
as 1, 2, 3, \ldots.

\subsection{Superscripts and Subcripts}
\label{sec:sas}
Let's consider something weird, like
$$x^2+y^{\frac{w}{v}} \stackrel{\infty}{\longrightarrow} x^{x^{x^{x}}}.$$ Or we
try to intergrate thus:
$$\int^3_1 \frac{x^3-x^4}{f'(x)g(x)} dx.$$ Being students in the mathematical
sciences, this formatting should look familiar from your exam papers in applied
mathematics, computer science, and pure mathematics \ldots{} maybe even operations
research and mathematical statistics. 

Maybe, we can borrow some notation from the theory around the Euler--Maclaurin
formula, for example,
$$S-I = \sum^p_{k=1}\frac{B_k}{k!}\left(f^{\left(k-1\right)}(n) -
f^{\left(k-1\right)}(m)\right) + R.$$ We could try to talk about the remainder
$R$, and get
$$|R| \leq \frac{2\zeta(2p)}{(2\pi)^{2p}}\int^n_m \left|f^{(2p)}(x)\right|dx.$$
Just as easily:
$$\sum^\infty_{k=0} \frac{1}{(z+k)^2}\sim\underbrace{\int^{\infty}_0
\frac{1}{(z+k)^2} dk}_{= \frac{1}{z}} + \frac{1}{2z^2} + \sum^{\infty}_{t=1}
\frac{B_{2t}}{z^{2t+1}}.$$
What about a bit of Cauchy?
$$\left| \frac{1}{2\pi{}i} \oint_C \frac{f(z)}{z-a}dz - f(a) \right| \leq
\max_{|z-a|=\varepsilon} |f(z) - f(a)| \stackrel{\varepsilon\rightarrow
0}{\longrightarrow}0.$$

\subsection{Operations}
\label{sec:operations}
Consider the following equations:
    $$\sum^n_{i=1} \frac{n!(2n\sqrt{y})}{5i^2} \ll
    \lim_{x\rightarrow{}\infty}\phi(x)\mbox{, and}$$
    $$$$
    $$\int^b_a f(x)\,dx \ge \lim_{x\rightarrow{}\infty}f(x_i)\frac{b-1}{n}.$$
Note the judicious use of the \texttt{mbox} command and thin spaces.

\subsection{Fractions}
\label{sec:fractions}
Let's see what we can do with fractions.
$$\frac{\frac{2\alpha}{x+y}+x^y}{x^y_1\div\frac{\sqrt{6}}{4+k}}\Rightarrow
\exists{}\; k:k < 1\%$$ Or, what about the following?
$$\sqrt{\frac{f(x)}{\frac{g(x)}{x^2}-1}+1} =
\sqrt[5]{\frac{f^2(x)}{\sqrt{g(x)!}}}$$

\subsection{Mixed Mathematical Structures}
\label{sec:mms}
$$u_1(x_1) \neq u_0(x_0)+\frac{(x_1-x_0)^2}{5!}f''(\xi)$$

For which values will the following be true?
$$x_1+x_2+x_3+\ldots+x_n = x_1\cdot x_2\cdot x_3\cdots x_n$$ 

Solve the next inequality:
    $$\sum^{\infty}_{\alpha{}=1}x^{\alpha} = \left.\oint^d_c f(x)dx \leq{}
    \prod^{\Theta}_{\gamma{}=1}\frac{f'''(x)}{\sqrt{(n+1)!}}\right|_{x=\lfloor
    x_0 \rfloor}$$

If you think the previous equation was difficult to type, try the next one:
    $$\left \lfloor \bigcap^{\circ}_{j=1} \int^{\infty}_{-\infty}
    \arccos{\left(\frac{\nabla{}\widetilde{x\pi_iy}}{x\underline{y}x}
    \right)}\right \rfloor^{\infty}_{j=1}$$

Compare the indents of the text lines above to those of \S\ref{sec:sas}. The
difference in indentation was accomplished simply by starting a new paragraph.
\end{document}
